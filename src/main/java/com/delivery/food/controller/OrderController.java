package com.delivery.food.controller;

import com.delivery.food.model.Order;
import com.delivery.food.model.Response;
import com.delivery.food.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class OrderController {

    @Autowired
    private OrderService orderService;

    @PostMapping("/order")
    public ResponseEntity<?> createOrder(@RequestBody @Valid Order order) {
        Response<String> response = new Response<>();
        String result = String.format("Order %s will get delivered in %.2f minutes.", order.getOrderId(), orderService.createOrder(order));
        response.setData(result);
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }
}
