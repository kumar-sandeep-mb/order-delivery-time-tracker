package com.delivery.food.store;

import com.delivery.food.model.Meal;
import com.delivery.food.model.MealType;
import com.delivery.food.model.Order;
import com.delivery.food.model.Restaurant;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Component
@Getter
@Slf4j
public class DataStore {
    private Map<Integer, Restaurant> restaurants;
    private Map<MealType, Meal> meals;
    private Map<String, Order> orders;

    @PostConstruct
    private void setRestaurants() {
        log.info("setting up mock data for system");
        restaurants = new HashMap<>();
        meals = new HashMap<>();

        //Initialize : no order in System
        orders = new HashMap<>();

        //Initialize : 1 restaurant in system
        Restaurant restaurant = new Restaurant(1, 7);
        restaurants.put(restaurant.getId(), restaurant);

        //Initialize : 2 Types of meal in system A and M
        Meal meal1 = buildMeal(MealType.A, 1, 17f);
        Meal meal2 = buildMeal(MealType.M, 2, 29f);

        meals.put(meal1.getMealType(), meal1);
        meals.put(meal2.getMealType(), meal2);
    }

    private Meal buildMeal(MealType mealType, int requiredCookingSlots, float requiredCookingTime) {
        Meal meal = new Meal();
        meal.setMealType(mealType);
        meal.setCookingSlotsRequired(requiredCookingSlots);
        meal.setCookingTimeInMinutes(requiredCookingTime);
        return meal;
    }

    public Restaurant getOrDefault(int restaurantId) {
        Restaurant restaurant = restaurants.get(restaurantId);
        if (restaurant == null) {
            restaurant = restaurants.get(1);
        }
        return restaurant;
    }

    public boolean orderExists(String id) {
        return orders.containsKey(id);
    }

    public boolean restaurantExists(int id) {
        return restaurants.containsKey(id);
    }

    public void saveOrder(Order order) {
        orders.put(order.getOrderId(), order);
    }
}
