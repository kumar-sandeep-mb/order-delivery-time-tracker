package com.delivery.food.scheduler;

import com.delivery.food.model.Order;
import com.delivery.food.model.Restaurant;
import com.delivery.food.store.DataStore;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;

@Slf4j
@Configuration
@EnableScheduling
public class ScheduledJob {

    @Autowired
    private DataStore dataStore;

    @Scheduled(cron = "0 * * * * *")
    public void updateOrderProcessingTime() {
        log.info("running scheduler to update orders");
        Map<Integer, Restaurant> restaurants = dataStore.getRestaurants();
        for (Restaurant restaurant : restaurants.values()) {
            PriorityQueue<Order> ordersUnderProcessing = restaurant.getOrderUnderProcessing();
            Queue<Order> queuedOrder = restaurant.getQueuedOrders();

            ordersUnderProcessing.forEach(order -> order.setProcessingTime(order.getProcessingTime() - 1));
            while (ordersUnderProcessing.peek() != null && ordersUnderProcessing.peek().getProcessingTime() <= 0) {
                Order order = ordersUnderProcessing.poll();
                log.info("order {} has been processed successfully from restaurant {}", order.getOrderId(), restaurant.getId());
            }

            queuedOrder.forEach(order -> {
                order.setProcessingTime(order.getProcessingTime() - 1);
                order.setWaitingTime(order.getWaitingTime() - 1);
            });

            while (queuedOrder.peek() != null && queuedOrder.peek().getWaitingTime() <= 0) {
                Order order = ordersUnderProcessing.poll();
                log.info("order {} sent for cooking from waiting queue of restaurant {}", order.getOrderId(), restaurant.getId());
                restaurant.prepareOrder(order, dataStore.getMeals());
            }
        }
    }
}
