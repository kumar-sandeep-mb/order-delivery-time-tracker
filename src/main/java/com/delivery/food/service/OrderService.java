package com.delivery.food.service;

import com.delivery.food.model.Order;
import com.delivery.food.model.Restaurant;
import com.delivery.food.store.DataStore;
import com.delivery.food.util.OrderProcessingTimeComparator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.Queue;

import static com.delivery.food.util.Constant.DEFAULT_RESTAURANT_ID;
import static com.delivery.food.util.Constant.MAX_PERMISSIBLE_ORDER_PROCESSING_TIME_IN_QUEUE;

@Service
public class OrderService {

    @Autowired
    private DataStore dataStore;

    @Autowired
    private CookingService cookingService;

    public float createOrder(Order order) {
        if (dataStore.orderExists(order.getOrderId())) {
            throw new HttpClientErrorException(HttpStatus.CONFLICT, "Order denied. Order already exist with orderId : " + order.getOrderId());
        }

        //Future : When user places an  order on any specific restaurant. Working with default restaurant  currently.
        int restaurantId = DEFAULT_RESTAURANT_ID;
        if (order.getRestaurant() != null) {
            if (!dataStore.restaurantExists(restaurantId)) {
                throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Order denied. Restaurant doesn't exist with restaurantId : " + restaurantId);
            }
            restaurantId = order.getRestaurant().getId();
        }
        Restaurant restaurant = dataStore.getOrDefault(restaurantId);

        if (!cookingService.orderAccommodable(order, restaurant)) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Order denied. Restaurant cannot accommodate order");
        }

        order.updateProcessingTime(dataStore.getMeals());
        float orderProcessingTime = calculateOrderProcessingTime(order, restaurant);
        if (orderProcessingTime > MAX_PERMISSIBLE_ORDER_PROCESSING_TIME_IN_QUEUE) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Order denied. Restaurant cannot accommodate order within 2 hours 30 minutes");
        }

        if (order.hasWaitingTime()) {
            restaurant.sendOrderToWaitingQueue(order);
        } else {
            restaurant.prepareOrder(order, dataStore.getMeals());
        }
        dataStore.saveOrder(order);
        return orderProcessingTime;
    }

    private float calculateOrderProcessingTime(Order order, Restaurant restaurant) {
        float waitingTime = 0f;
        if (!restaurant.ordersInQueue()) {
            if (!cookingService.freeCookingSlotsAvailable(order, restaurant)) {
                int availableFreeCookingSlots = restaurant.getAvailableFreeCookingSlotsCount();
                PriorityQueue<Order> orderUnderProcessing = restaurant.getOrderUnderProcessing();
                waitingTime = getWaitingTimeInQueue(orderUnderProcessing, order, availableFreeCookingSlots);
            }
        } else {
            Order lastOrderInWaitingQueue = restaurant.getQueuedOrders().peek();
            float minWaitingTime = lastOrderInWaitingQueue.getWaitingTime();
            waitingTime = minWaitingTime;

            PriorityQueue<Order> ordersUnderProcessAfterMinWaitingTime = getOrderUnderProcessAfterMinWaitingTime(restaurant, minWaitingTime);
            int engagedSlotsAfterMinWaitingTime = cookingService.getEngagedCookingSlotsForOrders(new ArrayList<>(ordersUnderProcessAfterMinWaitingTime));
            int availableFreeCookingSlotsAfterMinWaitingTime = restaurant.getTotalAvailableCookingSlots() - engagedSlotsAfterMinWaitingTime;

            if (availableFreeCookingSlotsAfterMinWaitingTime < cookingService.getCookingSlotsRequiredForOrder(order)) {
                waitingTime = getWaitingTimeInQueue(ordersUnderProcessAfterMinWaitingTime, order, availableFreeCookingSlotsAfterMinWaitingTime);
            }
        }

        order.setWaitingTime(waitingTime);
        return order.getProcessingTime() + waitingTime;
    }

    private float getWaitingTimeInQueue(PriorityQueue<Order> orders, Order order, int availableFreeCookingSlots) {
        float waitingTime = 0f;
        for (Order orderUnderProcess : orders) {
            availableFreeCookingSlots = availableFreeCookingSlots + cookingService.getCookingSlotsRequiredForOrder(orderUnderProcess);
            if (availableFreeCookingSlots >= cookingService.getCookingSlotsRequiredForOrder(order)) {
                waitingTime = orderUnderProcess.getProcessingTime();
                break;
            }
        }
        return waitingTime;
    }

    private PriorityQueue<Order> getOrderUnderProcessAfterMinWaitingTime(Restaurant restaurant, float minWaitingTime) {
        PriorityQueue<Order> orderUnderProcessing = restaurant.getOrderUnderProcessing();
        PriorityQueue<Order> ordersUnderProcessAfterMinWaitingTime = new PriorityQueue<>(new OrderProcessingTimeComparator());
        for (Order orderUnderProcess : orderUnderProcessing) {
            if (orderUnderProcess.getProcessingTime() > minWaitingTime) {
                ordersUnderProcessAfterMinWaitingTime.add(orderUnderProcess);
            }
        }

        Queue<Order> queuedOrders = restaurant.getQueuedOrders();

        for (Order queuedOrder : queuedOrders) {
            if (queuedOrder.getProcessingTime() > minWaitingTime) {
                ordersUnderProcessAfterMinWaitingTime.add(queuedOrder);
            }
        }
        return ordersUnderProcessAfterMinWaitingTime;
    }
}
