package com.delivery.food.service;

import com.delivery.food.model.MealType;
import com.delivery.food.model.Order;
import com.delivery.food.model.Restaurant;
import com.delivery.food.store.DataStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CookingService {

    @Autowired
    private DataStore dataStore;

    public int getCookingSlotsRequiredForOrder(Order order) {
        int cookingSlotsRequired = 0;
        for (MealType meal : order.getMeals()) {
            cookingSlotsRequired += dataStore.getMeals().get(meal).getCookingSlotsRequired();
        }
        return cookingSlotsRequired;
    }

    public boolean orderAccommodable(Order order, Restaurant restaurant) {
        int requiredSlots = getCookingSlotsRequiredForOrder(order);
        int totalAvailableCookingSlots = restaurant.getTotalAvailableCookingSlots();
        return requiredSlots <= totalAvailableCookingSlots;
    }

    public boolean freeCookingSlotsAvailable(Order order, Restaurant restaurant) {
        int requiredSlots = getCookingSlotsRequiredForOrder(order);
        int availableFreeCookingSlots = restaurant.getAvailableFreeCookingSlotsCount();
        return requiredSlots <= availableFreeCookingSlots;
    }

    public int getEngagedCookingSlotsForOrders(List<Order> orders) {
        int engagedSlots = 0;
        for (Order orderUnderProcessAfterMinWaitingTime : orders) {
            engagedSlots += getCookingSlotsRequiredForOrder(orderUnderProcessAfterMinWaitingTime);
        }
        return engagedSlots;
    }
}
