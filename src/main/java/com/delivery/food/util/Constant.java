package com.delivery.food.util;

public class Constant {
    public static final float MAX_PERMISSIBLE_ORDER_PROCESSING_TIME_IN_QUEUE = 150f;
    public static final int DEFAULT_RESTAURANT_ID = 1;

    private Constant() {
    }
}
