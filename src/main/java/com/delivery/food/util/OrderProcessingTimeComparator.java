package com.delivery.food.util;

import com.delivery.food.model.Order;

import java.util.Comparator;

public class OrderProcessingTimeComparator implements Comparator<Order> {

    @Override
    public int compare(Order orderUnderProcess1, Order orderUnderProcess2) {
        return Float.compare(orderUnderProcess1.getProcessingTime(), orderUnderProcess2.getProcessingTime());
    }
}
