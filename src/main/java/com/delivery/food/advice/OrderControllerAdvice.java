package com.delivery.food.advice;

import com.delivery.food.model.Response;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpStatusCodeException;

@ControllerAdvice
public class OrderControllerAdvice {

    @ExceptionHandler(HttpStatusCodeException.class)
    public ResponseEntity<?> handleHttpStatusCodeException(HttpStatusCodeException e) {
        Response<String> response = new Response<>();
        response.setError(e.getMessage());
        return ResponseEntity.status(e.getStatusCode()).body(response);
    }
}
