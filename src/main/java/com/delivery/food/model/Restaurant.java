package com.delivery.food.model;

import com.delivery.food.util.OrderProcessingTimeComparator;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

@Data
@Getter
@Setter
@Slf4j
public class Restaurant {

    private final int id;
    private List<CookingSlot> cookingSlots;
    private Queue<Order> queuedOrders;
    private PriorityQueue<Order> orderUnderProcessing;

    public Restaurant(int id, int numberOfCookingSlots) {
        this.id = id;
        cookingSlots = new ArrayList<>(numberOfCookingSlots);
        for (int i = 0; i < numberOfCookingSlots; i++) {
            CookingSlot cookingSlot = new CookingSlot(1 + 1);
            cookingSlots.add(cookingSlot);
        }
        queuedOrders = new ArrayDeque<>();
        orderUnderProcessing = new PriorityQueue<>(new OrderProcessingTimeComparator());
    }

    public int getTotalAvailableCookingSlots() {
        return cookingSlots.size();
    }

    public int getAvailableFreeCookingSlotsCount() {
        int availableSlots = 0;
        for (CookingSlot slot : cookingSlots) {
            if (slot.getOrderUnderCooking() == null) {
                availableSlots++;
            }
        }
        return availableSlots;
    }

    public List<CookingSlot> getAvailableFreeCookingSlots() {
        List<CookingSlot> freeSlots = new ArrayList<>();
        for (CookingSlot slot : cookingSlots) {
            if (slot.getOrderUnderCooking() == null) {
                freeSlots.add(slot);
            }
        }
        return freeSlots;
    }

    public boolean ordersInQueue() {
        return !queuedOrders.isEmpty();
    }

    public void prepareOrder(Order order, Map<MealType, Meal> meals) {
        log.info("order {} arrived for cooking in restaurant {}", order.getOrderId(), id);
        List<MealType> mealsInOrder = order.getMeals();
        List<CookingSlot> freeSlots = getAvailableFreeCookingSlots();
        int freeSlot = 0;
        for (MealType meal : mealsInOrder) {
            int slotsRequired = meals.get(meal).getCookingSlotsRequired();
            for (int i = 0; i < slotsRequired; i++) {
                freeSlots.get(freeSlot).setOrderUnderCooking(order);
                freeSlot++;
            }
        }
        orderUnderProcessing.add(order);
    }

    public void sendOrderToWaitingQueue(Order order) {
        log.info("order {} inserted into waiting queue in restaurant {}", order.getOrderId(), id);
        queuedOrders.add(order);
    }

    @Data
    @Getter
    @Setter
    class CookingSlot {
        int cookingSlotId;
        Order orderUnderCooking;

        CookingSlot(int cookingSlotId) {
            this.cookingSlotId = cookingSlotId;
        }
    }
}
