package com.delivery.food.model;

import com.delivery.food.validation.Id;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

@Data
@Getter
@Setter
public class Order {
    @Id(message = "OrderId must be in 2 digit format")
    private final String orderId;
    private final User user;
    private final Restaurant restaurant;
    @NotNull(message = "Order must have at least 1 meal")
    private final List<MealType> meals;
    @Min(value = 0, message = "Distance shouldn't be negative")
    private final float distance;
    private float waitingTime;
    private float processingTime;
    private float maxCookingTime;

    public void updateProcessingTime(Map<MealType, Meal> availableMeals) {
        for (MealType meal : meals) {
            if (availableMeals.get(meal).getCookingTimeInMinutes() > maxCookingTime) {
                maxCookingTime = availableMeals.get(meal).getCookingTimeInMinutes();
            }
        }
        processingTime = maxCookingTime + distance * 8;
    }

    public boolean hasWaitingTime() {
        return waitingTime > 0;
    }
}
