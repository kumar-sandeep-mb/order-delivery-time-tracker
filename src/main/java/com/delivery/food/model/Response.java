package com.delivery.food.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class Response<T> {
    private T data;
    private T error;
}
