package com.delivery.food.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class Meal {
    private MealType mealType;
    private float cookingTimeInMinutes;
    private int cookingSlotsRequired;
}
