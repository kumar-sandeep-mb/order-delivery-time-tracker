package com.delivery.food.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class IdValidator implements ConstraintValidator<Id, String> {
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        boolean result;
        if (value.isEmpty()) {
            result = false;
        } else {
            int size = value.length();
            if (size != 2) {
                result = false;
            } else {
                result = Character.isDigit(value.charAt(0)) && Character.isDigit(value.charAt(1));
            }
        }
        return result;
    }
}
